<?php
require 'vendor/autoload.php';

$loop = new React\EventLoop\StreamSelectLoop();
$dnode = new DNode\DNode($loop);

$dnode = new DNode\DNode($loop);
$dnode->connect(1337, function($remote, $connection) {
    $remote->zing(5, function($n) use ($connection) {
        echo "n = {$n}\n";
        $connection->end();
    });
});

$loop->run();